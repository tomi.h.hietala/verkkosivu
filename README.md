﻿# Verkkosivu sunnitelma

Tarkoituksena on suunnitella verkkokauppa tyylistä sivua jolla myydään pelitilejä.
Verkkosivu toimii "onepage" tyylillä, eli sivu koostuu vain yhdestä sivusta.


## Kuvat

Sivulla on 4 keskikokoista kuvaa 300x300.
5 suurta kuvaa joiden korkeus 1000px.
Pieni kuva joka toimii kuvallisena linkkinä.

## Tekstiä sivulle

### Home

MapleRoyals Account Services

### About Me

I am a longterm MapleStory player. First touch with Maplestory was roughly 10 years ago. I have been a part of MapleStory hacking community for as long as I can remember. Now I have moved my services to MapleRoyals. At the moment I only offer accounts services. Item services will come SoonTM. 

### Available Classes

#### Magician 
The Magician may look fragile, but this powerful magic-user can obliterate enemies from afar with devastating spell

#### Thief
Thieves are born sneaks. They're skilled at dodging attacks and are particularly fond of stealth, using their abilities to evade enemies or hide from them entirely.

#### Bowman
Bowmen are nimble, long-range snipers. They have a variety of skills to help attack, evade danger, buff their damage, and launch arrows at groups of monsters

#### Warrior
The Warrior is a solid, well-rounded character perfect for players new to MapleStory. This character excels at defense, with large amounts of health and access to powerful suits of armor.

#### Pirate
Ahoy, mateys! If it's adventure ye be seeking, then the Pirate is the class for you! These swashbuckling rogues come equipped with guns and brass knuckles, and can specialize as either a Buccaneer or Corsair depending on their preference.


### Custom Orders

Orders are made upon purchased. Delivering the account takes 1-2 weeks depending of the difficulty of the task.
Complete HP washing isn't currently possible, but gaining high extraMP pool is possible, meaning accounts will be grinded with 100-250 base int depending of how much extraMP you wish for the account to have.
 Base int will not be reset before you receive the account. Below I have listed prices for the most common combinations of level, job and amount of INT.

 #### TAB SYSTEM:  Default text for each tab

 If the avaibility is 0, it means there is currently 0 accounts with that job on stock. These are custom orders which means that the availability is 
almost certainly always 0. Accounts are moved to the level up queue when the payment has been received. You will receive an email about your order status once its level up process begins. Finishing the order usually takes about a 1-2 weeks.

#### Yhteystiedot
Yhteystiedot eivät oikein sopineet sivun tarkoitukseen.